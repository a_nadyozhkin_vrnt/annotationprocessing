package org.artyom.annotationprocessing;

import org.artyom.annotationprocessing.factory.ZzooFactory;

public class Main {

  public static void main(String[] args) {

    ZzooFactory zzooFactory = new ZzooFactory("org.artyom.annotationprocessing.handler");
    zzooFactory.getHandlers().get("maintainerMessageHandler").handle();
  }
}
