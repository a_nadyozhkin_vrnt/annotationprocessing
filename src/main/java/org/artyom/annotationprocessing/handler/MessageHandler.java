package org.artyom.annotationprocessing.handler;

public interface MessageHandler {

  void handle();
}
