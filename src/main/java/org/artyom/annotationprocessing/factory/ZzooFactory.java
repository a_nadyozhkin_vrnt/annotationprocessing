package org.artyom.annotationprocessing.factory;

import lombok.Getter;
import org.artyom.annotationprocessing.annotation.ZzooHandler;
import org.artyom.annotationprocessing.handler.AbstractHandler;
import org.artyom.annotationprocessing.handler.MessageHandler;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class ZzooFactory {

  @Getter
  private Map<String, MessageHandler> handlers;


  public ZzooFactory(String basePackage) {
    this.handlers = new HashMap<>();
    instantiate(basePackage);
  }


  private void instantiate(String basePackage) {

    System.out.println("basePackage: " + basePackage);
    try {
      ClassLoader classLoader = ClassLoader.getSystemClassLoader();

      String path = basePackage.replace('.', '/');
      System.out.println("path: " + path);
      Enumeration<URL> resources = classLoader.getResources(path);

      while (resources.hasMoreElements()) {
        URL resource = resources.nextElement();
        File file = new File(resource.toURI());

        for (File classFile : file.listFiles()) {
          String fileName = classFile.getName();//ProductService.class
          System.out.println("fileName: " + fileName);
          System.out.println(fileName);
          if (fileName.endsWith(".class")) {
            String className = fileName.substring(0, fileName.lastIndexOf("."));

            Class classObject = Class.forName(basePackage + "." + className);

            if (classObject.isAnnotationPresent(ZzooHandler.class)) {
//              if (MessageHandler.class.isAssignableFrom(classObject)) {

              boolean is = MessageHandler.class.isAssignableFrom(classObject);
              System.out.println("implements messhandlr interface: " + is);
              System.out.println("Component: " + classObject);

              Object instance = classObject.newInstance();
              String beanName = className.substring(0, 1).toLowerCase() + className.substring(1);
              System.out.println("bean name: " + beanName);
              handlers.put(beanName, (MessageHandler) instance);
            }
          }
        }
      }
    } catch (IOException | URISyntaxException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
    }
  }
}
